
import java.util.ArrayList;


public class FairReadWriteLock {
	private int numReaders;
	private int numWriters;	
	private ArrayList<ThreadMonitor> requests = new ArrayList<ThreadMonitor>();
	
	
	public FairReadWriteLock() {
		numReaders = 0;
		numWriters = 0;
	}
	
	/**
	 * A simple monitor object to clean up all the synchronized blocks in 
	 *   beginRead() and endRead();
	 *   
	 * @author Brad Stewart, Chris McNair
	 *
	 */
	private class ThreadMonitor {

		private boolean canWrite = false;
		
		public synchronized void doWait() throws InterruptedException {
		    while(!canWrite){
		        this.wait();
		    }
		    this.canWrite = false;
		  }

		  public synchronized void doNotify() {
		    this.canWrite = true;
		    this.notify();
		  }

		  public boolean equals(Object o) {
		    return this == o;
		  }

	}
	
	/**
	 * Clients should call this method to obtain read access to the resource.
	 *   - Note: Method is not synchronized so the monitors can be created, then
	 *           necessary sections are synchronized within the method.
	 */
	public void beginRead() {

		ThreadMonitor monitor = new ThreadMonitor();
		getInLine(monitor);
		
		while( isBlockedFromReading(monitor) ) {
			try {
				monitor.doWait();
			} catch (InterruptedException e) {
				synchronized(this) { requests.remove(monitor); }
				e.printStackTrace();		
			}
		}		
	}
	
	/**
	 * Synchronized method to check conditions for allowing a reader into the resource.
	 * 
	 * @param monitor
	 * @return
	 */
	private synchronized boolean isBlockedFromReading(ThreadMonitor monitor) {
		boolean isBlocked = numWriters > 0 || requests.get(0) != monitor;
		
		if ( !isBlocked ) {
			numReaders++;
			requests.remove(monitor);
		}
		
		return isBlocked;
	}

	/**
	 * Synchronized method to finish reading the resource;
	 */
	public synchronized void endRead() {
	    numReaders--;
	    notifyFirst();
	}
	
	public void beginWrite() {
		ThreadMonitor monitor = new ThreadMonitor();		
		getInLine(monitor);
				
		while ( isBlockedFromWriting( monitor ) ) {
			try {
				monitor.doWait();
			} catch (InterruptedException e) {
				synchronized(this) { requests.remove(monitor); }
				e.printStackTrace();
			}
				
		}
		
	}
	
	private synchronized boolean isBlockedFromWriting( ThreadMonitor monitor ) {
		boolean isBlocked = numReaders > 0 || numWriters > 0 || requests.get(0) != monitor;
		
		if ( !isBlocked ) {
			numWriters++;
			requests.remove(monitor);
		}
		
		return isBlocked;
	}
	
	public synchronized void endWrite() {
		numWriters--;
		notifyFirst();
	}
	
	/*
	 * Notify the first ThreadMonitor in this object's thread queue.
	 */
	private void notifyFirst() {
		if ( requests.size() > 0 ) {
			requests.get(0).doNotify();
		}
	}
	
	/*
	 * Synchronized addition of a ThreadMonitor to the queue.
	 * 
	 * @param monitor
	 */
	private synchronized void getInLine( ThreadMonitor monitor ) {
		requests.add(monitor);
	}
	
}
