import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;


public class CyclicBarrier {
	Semaphore door1, door2;
	final int numThreads;
	AtomicInteger arrived_at_front_door, internal_count;
	
	public CyclicBarrier(int parties) {
		// create two binary semaphores initialized to true
		door1 = new Semaphore(1);
		door2 = new Semaphore(1);
		try {
			// close first door
			door1.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}			
		this.numThreads = parties;
		this.arrived_at_front_door = new AtomicInteger(0);
		this.internal_count = new AtomicInteger(0);
	}
	
	public int await() throws InterruptedException {
		int index = numThreads - arrived_at_front_door.incrementAndGet();

		while (door1Closed() && 
				(internal_count.get() != 0)) {
			// wait at front door while there are threads from 
			// previous round in the barrier
			;
		}
		// assert: all threads have exited barrier
		
		// If you are the first thread entering door1, 
		// close door2, open door1.
		if (internal_count.getAndIncrement() == 0) {
			door2.acquire();
			door1.release();
		}
		
		while (door1Closed()) {
			;	// wait while the first thread opens the door
		}
		
		// Last person in closes door1 behind them and opens door2
		if (internal_count.get() == numThreads) {
			door1.acquire();
			arrived_at_front_door.set(0);
			door2.release();
		}
		
		while ((door2Closed()) && (internal_count.get() != numThreads)) {
			;	// do nothing until last thread arrives
		}
		// assert: Every thread has arrived in the barrier
		
		while (door2Closed()) {
			;	// wait while the last thread in closes door1 and opens door2
		}
		
		internal_count.getAndDecrement();
		return index;
	}
	
	boolean door1Closed() {
		return (door1.availablePermits() == 0);
	}
	
	boolean door1Open() {
		return !door1Closed();
	}
	
	boolean door2Closed() {
		return (door2.availablePermits() == 0);
	}
	
	boolean door2Open() {
		return !door2Closed();
	}
}