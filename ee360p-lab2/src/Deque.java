import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;


public class Deque {
	final ReentrantLock monitorLock = new ReentrantLock();
	final Condition notFull = monitorLock.newCondition();
	final Condition notEmpty = monitorLock.newCondition();
	
	LinkedList<Integer> list = new LinkedList();
	final int capacity;
	
	public Deque(int capacity) {
		this.capacity = capacity;
	}
	
	public int getHead() {
		monitorLock.lock();
		try {
			while(list.isEmpty()) {
				notEmpty.await();
			}
			return list.getFirst();
		} catch (InterruptedException e) {
			return 0;
		}
		finally {
			monitorLock.unlock();
		}
	}
	
	public void insertHead(int value) {
		monitorLock.lock();
		try {
			while (list.size() == capacity) {
				notFull.await();
			}
			list.addFirst(value);
			notEmpty.signal();
		} catch (InterruptedException e) {
		} finally {
			monitorLock.unlock();
		}
	}
	
	public int deleteHead() {
		monitorLock.lock();
		try {
			while (list.isEmpty()) {
				notEmpty.await();
			}
			int val = list.removeFirst();
			notFull.signal();
			return val;
		} catch (InterruptedException e) {
			return 0;
		} finally {
			monitorLock.unlock();
		}
	}
	
	public int getTail() {
		monitorLock.lock();
		try {
			while(list.isEmpty()) {
				notEmpty.await();
			}
			return list.getLast();
		} catch (InterruptedException e) {
			return 0;
		}
		finally {
			monitorLock.unlock();
		}
	}
	
	public void insertTail(int value) {
		monitorLock.lock();
		try {
			while (list.size() == capacity) {
				notFull.await();
			}
			list.addLast(value);
			notEmpty.signal();
		} catch (InterruptedException e) {
		} finally {
			monitorLock.unlock();
		}
	}
	
	public int deleteTail() {
		monitorLock.lock();
		try {
			while (list.isEmpty()) {
				notEmpty.await();
			}
			int val = list.removeLast();
			notFull.signal();
			return val;
		} catch (InterruptedException e) {
			return 0;
		} finally {
			monitorLock.unlock();
		}
	}
	
	public String toString() {
		monitorLock.lock();
		try {
			StringBuilder sb = new StringBuilder();
			sb.append("[ ");
			Iterator<Integer> it = list.listIterator();
			while (it.hasNext()) {
				sb.append(it.next().toString() + ' ');
			}
			sb.append(']');
			return sb.toString();
		} finally {
			monitorLock.unlock();
		}
	}
}
