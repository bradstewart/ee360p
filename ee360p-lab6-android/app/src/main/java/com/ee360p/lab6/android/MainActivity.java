package com.ee360p.lab6.android;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.appspot.brad_chris_ee360p.api.Api;
import com.appspot.brad_chris_ee360p.api.model.CheckIn;
import com.appspot.brad_chris_ee360p.api.model.CheckInCollection;
import com.google.common.base.Strings;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class MainActivity extends Activity {

    private static final String LOG_TAG = "MainActivity";
//    private CheckInsDataAdapter mListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private Location getLocation() {
        LocationManager locMan = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        Location loc = locMan.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        return loc;
    }

    public void onClickCheckIn( View view ) {
        View rootView = view.getRootView();

        // Get text from the input box
        TextView nameInputText = (TextView)rootView.findViewById(R.id.name_edit_text);
        if (nameInputText.getText()==null || Strings.isNullOrEmpty(nameInputText.getText().toString())) {
            Toast.makeText(this, "Input a name", Toast.LENGTH_SHORT).show();
            return;
        } else {
            Toast.makeText(this, "Checking in...", Toast.LENGTH_SHORT).show();
        }

        // Get the devices location
        LocationManager locMan = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        Location loc = locMan.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

        final Double latitude = loc.getLatitude();
        final Double longitude = loc.getLongitude();
        final String username = nameInputText.getText().toString();

//        CheckIn newCheckIn = new CheckIn(latitude, longitude, nameInputText.getText() );

        AsyncTask<Integer, Void, CheckIn> sendCheckIn =
                new AsyncTask<Integer, Void, CheckIn> () {
                    @Override
                    protected CheckIn doInBackground(Integer... integers) {
                        // Retrieve service handle.
                        Api apiServiceHandle = AppConstants.getApiServiceHandle();

                        try {
                            Api.Checkins.Create createCheckIn = apiServiceHandle.checkins().create( latitude, longitude, username );
                            CheckIn checkin = createCheckIn.execute();
                            return checkin;
                        } catch (IOException e) {
                            Log.e(LOG_TAG, "Exception during API call", e);
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(CheckIn checkin) {
                        if (checkin!=null) {
                            successfulCheckin(checkin);
                        } else {
                            Log.e(LOG_TAG, "Null response from the API.");
                        }
                    }
                };

        sendCheckIn.execute();
    }

    private void successfulCheckin( CheckIn checkin ) {
        Toast.makeText(this, checkin.getUsername() + "checked in successfully", Toast.LENGTH_SHORT).show();
    }

    public void onClickDisplayNearby( View view ) {

        Toast.makeText(this, "Loading checkins...", Toast.LENGTH_SHORT).show();

        LocationManager locMan = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        Location loc = locMan.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

        final Double latitude = loc.getLatitude();
        final Double longitude = loc.getLongitude();

        AsyncTask<Integer, Void, CheckInCollection> sendCheckIn =
                new AsyncTask<Integer, Void, CheckInCollection> () {
                    @Override
                    protected CheckInCollection doInBackground(Integer... integers) {
                        // Retrieve service handle.
                        Api apiServiceHandle = AppConstants.getApiServiceHandle();

                        try {
                            Api.Checkins.Nearby nearbyCheckins = apiServiceHandle.checkins().nearby(latitude, longitude);
                            CheckInCollection checkin = nearbyCheckins.execute();
                            return checkin;
                        } catch (IOException e) {
                            Log.e(LOG_TAG, "Exception during API call", e);
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(CheckInCollection checkins) {
                        if (checkins!=null) {
                            displayCheckIns(checkins);
                        } else {
                            Log.e(LOG_TAG, "Null response from the API.");
                        }
                    }
                };

        sendCheckIn.execute();
    }

    private void displayCheckIns(CheckInCollection checkins) {
        String msg;
        if (checkins==null || checkins.size() < 1) {
            msg = "Checkins were not found.";
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
        } else {
            Log.d(LOG_TAG, "Displaying " + checkins.size() + " checkins.");

            List<CheckIn> checkinList = checkins.getItems();
            TextView listText = (TextView) findViewById(R.id.checkin_list_view);

            StringBuilder sb = new StringBuilder();
            for ( CheckIn checkin : checkinList ) {
                sb.append( checkin.getUsername() + "\n" );
            }
            listText.setText( sb.toString() );
        }
    }



}
