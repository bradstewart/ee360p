import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertArrayEquals;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author Brad Stewart (bjs2639), Chris McNair (cm38329)
 * 
 * Test cases for PSort.java
 *
 */
public class TestPSort {
	private static ExecutorService executorService;

	@Before
	public void setUp() throws Exception {
		executorService = Executors.newCachedThreadPool();
	}

	@After
	public void tearDown() throws Exception {
		executorService.shutdown();
	}

	@Test
	public void testPSort10k() throws InterruptedException, ExecutionException {
		int[] actual = populateRandomArray(10000, Integer.MAX_VALUE, true);
		int[] expected = Arrays.copyOf(actual, actual.length);
		
//		Future<?> result = executorService.submit( new PSort(actual, 0, actual.length) );		
//		while ( result.get() != null ) { }
		PSort.parallelSort(actual, 0, actual.length);
				
		verify(actual, expected);
	}
	
	@Test
	public void testPSort10kNoNegs() throws InterruptedException, ExecutionException {
		int[] actual = populateRandomArray(10000, Integer.MAX_VALUE, false);
		int[] expected = Arrays.copyOf(actual, actual.length);
				
		PSort.parallelSort(actual, 0, actual.length);
		
		verify(actual, expected);
	}
	
	@Test
	public void testPSort10kMax10NoNegs() throws InterruptedException, ExecutionException {
		int[] actual = populateRandomArray(10000, 10, false);
		int[] expected = Arrays.copyOf(actual, actual.length);
				
		PSort.parallelSort(actual, 0, actual.length);
		
		verify(actual, expected);
	}
	
	@Test
	public void testPSort10kMax37() throws InterruptedException, ExecutionException {
		int[] actual = populateRandomArray(10000, 37, true);
		int[] expected = Arrays.copyOf(actual, actual.length);
				
		PSort.parallelSort(actual, 0, actual.length);
		
		verify(actual, expected);
	}
	
	/**
	 * Create and populate an integer array of specified size with random integers.
	 * 
	 * @param size
	 * @param max		Maximum value for the random integers
	 * @param inclNegs	Include negative values
	 * @return
	 */
	private int[] populateRandomArray(int size, int max, boolean inclNegs ) {
		int[] array = new int[size];
		Random rand = new Random();
	    for (int i=0; i<array.length; i++){
	        array[i] = inclNegs ? (rand.nextInt() % max) : rand.nextInt(max);
	    }
	    return array;   
	}
	
	/**
	 * Sorts a copy of the original array with Java's Array API sort() method.
	 * 
	 * @param actual	Sorted array (by the algo under test)
	 * @param expected  Unsorted copy of the original array
	 */
	private void verify(int[] actual, int[] expected) {		
		Arrays.sort(expected);		
//		System.out.println(Arrays.toString(actual));
//		System.out.println(Arrays.toString(expected));
		assertTrue(Arrays.equals(actual, expected));
	}

}
