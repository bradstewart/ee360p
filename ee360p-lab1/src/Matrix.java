import java.util.concurrent.*;
import java.util.*;

/**
 * 
 * @author Brad Stewart (bjs2639), Chris McNair (cm38329)
 * 
 * Multi-threaded matrix multiplication using a FixedThreadPool. Client code should 
 *   use the MatrixMult static method.
 *
 */
public class Matrix implements Callable<Double> {
	private static int rowA, rowB, colA, colB;
	private static double [][]A;
	private static double [][]B;
	private static double [][]C;
	private int i, j;
	
	public Matrix(int i, int j) {
		this.i = i;
		this.j = j;
	}
	
	/**
	 * Multithreaded multiply two matrices represented as double[][] 2D arrays.
	 * 
	 * @param rowA			number of rows in matrix A
	 * @param colA			number of cols in matrix A, must equal rowB
	 * @param A				matrix A
	 * @param rowB			number of rows in Matrix B, must equal colA
	 * @param colB			number of cols in Matrix B
	 * @param B				matrix B
	 * @param C				location for the result	
	 * @param numThreads	
	 * @return				True if successful
	 */
	public static boolean MatrixMult(int rowA, int colA, double[][] A,
									 int rowB, int colB, double[][] B,
									 double[][] C, int numThreads) {
		if ((colA != rowB) || (numThreads == 0)) {
			return false;
		}
		Matrix.rowA = rowA;
		Matrix.colA = colA;
		Matrix.rowB = rowB;
		Matrix.colB = colB;
		Matrix.A = A;
		Matrix.B = B;
		Matrix.C = C;
		int entries = rowA*colB;
		
		ExecutorService threadPool = Executors.newFixedThreadPool(numThreads);
		// store the futures while they are computed so we can access later
		ArrayList<Future<Double>> futureC = new ArrayList<Future<Double>>(entries);
		
		// start the threads for each element in C
		for(int x = 0; x < rowA; x++) {
			for (int y = 0; y < colB; y++) {
				Future<Double> f = threadPool.submit(new Matrix(x,y));
				futureC.add(f);
			}
		}
		
		// fill in C with the list of futures
		try {
			int listIndex = 0;
			for(int x = 0; x < rowA; x++) {
				for (int y = 0; y < colB; y++) {
					C[x][y] = futureC.get(listIndex++).get();
				}
			}
		} catch (Exception e) { System.err.println (e); return false;}
		
		return true;
	}

	// compute an element of C
	@Override
	public Double call() throws Exception {
		try {
			double sum = 0;
			for(int x = 0, y = 0; x < colA; x++, y++) {
				sum += A[i][x] * B[y][j];
			}
			return sum;
		} catch (Exception e) { System.err.println (e); return null;}
	}
	
	public static void main(String[] args) {
	    try {
	    	
	    	double[][] A = { {1,6,5,1}, {2,4,2,2}, {5,3,2,2} };
	    	double[][] B = { {1,4}, {2,4}, {3,2}, {1,2} };
	    	int rowA = A.length, colA = A[0].length, rowB = B.length, colB = B[0].length;
	    	double[][] C = new double[rowA][colB];
	    	
	        if (MatrixMult(rowA,colA,A,rowB,colB,B,C,4)) {
	        	for (double[] arr : C) {
	                System.out.println(Arrays.toString(arr));
	            }
	        }
	    } catch (Exception e) { System.err.println (e); }
	  }
}
