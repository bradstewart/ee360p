import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author Brad Stewart (bjs2639), Chris McNair (cm38329)
 * 
 * Test cases for Matrix.java
 *
 */
public class TestMatrix {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSquare() {
		double[][] A = populateRandomMatrix( 20, 20, Integer.MAX_VALUE, true );
		double[][] B = populateRandomMatrix( 20, 20, Integer.MAX_VALUE, true );
		
		executeAndVerify(A, B, 4);
	}
	
	@Test
	public void testSquarePosNeg() {
		double[][] A = populateRandomMatrix( 20, 20, Integer.MAX_VALUE, false );
		double[][] B = populateRandomMatrix( 20, 20, Integer.MAX_VALUE, true );
		
		executeAndVerify(A, B, 6);
	}
	
	@Test
	public void testRectangle() {
		double[][] A = populateRandomMatrix( 3, 4, Integer.MAX_VALUE, true );
		double[][] B = populateRandomMatrix( 4, 7, Integer.MAX_VALUE, true );
		
		executeAndVerify(A, B, 8);
	}
	
	@Test
	public void testSmallValues() {
		double[][] A = populateRandomMatrix( 13, 4, 1, false );
		double[][] B = populateRandomMatrix( 4, 7, 0, true );
		
		executeAndVerify(A, B, 1);
	}

	@Test
	public void test3x2() {
		double[][] A = { {1,6,5,1}, {2,4,2,2}, {5,3,2,2} };
    	double[][] B = { {1,4}, {2,4}, {3,2}, {1,2} };

    	int rowA = A.length, colA = A[0].length, rowB = B.length, colB = B[0].length;
    	double[][] C = new double[rowA][colB];
    	double[][] oracle = { {29.0,40.0}, {18.0,32.0}, {19.0,40.0} };
    	
    	assertTrue(Matrix.MatrixMult(rowA,colA,A,rowB,colB,B,C,4));
    	int row = 0;
    	for (double[] arr : C) {
    		assertTrue(Arrays.equals(arr,oracle[row++]));
        }
	}
	
	@Test
	public void testInvalidSize() {
		double[][] A = { {1,6,5}, {2,4,2}, {5,3,2} };
    	double[][] B = { {1,4}, {2,4}, {3,2}, {1,2} };
    	int rowA = A.length, colA = A[0].length, rowB = B.length, colB = B[0].length;
    	double[][] C = new double[rowA][colB];
    	
    	assertFalse(Matrix.MatrixMult(rowA,colA,A,rowB,colB,B,C,4));
	}
	
	/**
	 * Populate a matrix with random doubles for testing purposes.
	 * 
	 * @param numCols			
	 * @param numRows
	 * @param maxVal
	 * @param inclNegs
	 * @return
	 */
	private double[][] populateRandomMatrix(int numCols, int numRows, int maxVal, boolean inclNegs ) {		
		double min = inclNegs ? Integer.MIN_VALUE : 0.0;		
		double[][] matrix = new double[numCols][numRows];
		Random rand = new Random();
		
	    for (int i=0; i<matrix.length; i++){	        
	        for (int j=0; j<matrix[i].length; j++) {
	        	matrix[i][j] = min + (maxVal - min) * rand.nextDouble();
	        }
	    }
	    return matrix;   
	}
	
	/**
	 * Naive matrix multiply for result comparison.
	 * 
	 * @param A	
	 * @param B	
	 * @return
	 */
	private double[][] multiply( double[][] A, double[][] B ) {
        int rowA = A.length;
        int colA = A[0].length;
        int rowB = B.length;
        int colB = B[0].length;
        if (colA != rowB) throw new RuntimeException("Illegal matrix dimensions.");
        double[][] C = new double[rowA][colB];
        for (int i = 0; i < rowA; i++)
            for (int j = 0; j < colB; j++)
                for (int k = 0; k < colA; k++)
                    C[i][j] += (A[i][k] * B[k][j]);
        return C;
	}
	
	/**
	 * Executes multithreaded MatrixMult, singlethreaded multiply, and compares the results.
	 * 
	 * @param A
	 * @param B
	 * @param numThreads
	 */
	private void executeAndVerify(double[][] A, double[][] B, int numThreads) {
		double[][] actual = new double[A.length][B[0].length];
		double[][] expected = multiply( A, B );
		
		Matrix.MatrixMult(A.length, A[0].length, A, B.length, B[0].length, B, actual, numThreads);
		
		for (int i=0; i < actual.length; i++) {
			
			if (!Arrays.equals(actual[i], expected[i])) {
				System.out.println("Threaded:     " + Arrays.toString(actual[i]));
				System.out.println("Non-Threaded: " + Arrays.toString(expected[i]));
				fail();
			}
		}
	}
	
	/**
	 * Print a matrix to the console.
	 * 
	 * @param matrix
	 */
	private void printMatrix( double[][] matrix ) {
		for (int i=0; i < matrix.length; i++) {
			System.out.println("  "+i+"  "+Arrays.toString(matrix[i]) );
		}
	}
	

}
