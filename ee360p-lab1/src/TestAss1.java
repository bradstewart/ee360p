import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestAss1 {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void correctSort() {
		int[] A = new int[10000];
		int[] testCopy = new int[10000];
		PSort.populateArray(A);
		System.arraycopy(A,0,testCopy,0,A.length);
		
		assertTrue(Arrays.equals(A,testCopy));
		
		Thread t1 = new Thread(new PSort(A,0,A.length));
		t1.start();
		
		try {
			t1.join();
		} catch (Exception e) {}
		
		assertFalse(Arrays.equals(A,testCopy));
		
		Arrays.sort(testCopy);
		assertTrue(Arrays.equals(A,testCopy));		
	}
	
	@Test
	public void staticCallSort() {
		int[] A = new int[10000];
		int[] testCopy = new int[10000];
		PSort.populateArray(A);
		System.arraycopy(A,0,testCopy,0,A.length);
		
		assertTrue(Arrays.equals(A,testCopy));
		
		PSort.parallelSort(A,0,A.length);
		
		assertFalse(Arrays.equals(A,testCopy));
		
		Arrays.sort(testCopy);
		assertTrue(Arrays.equals(A,testCopy));		
	}
	
	@Test
	public void test3x2() {
		double[][] A = { {1,6,5,1}, {2,4,2,2}, {5,3,2,2} };
    	double[][] B = { {1,4}, {2,4}, {3,2}, {1,2} };
    	int rowA = A.length, colA = A[0].length, rowB = B.length, colB = B[0].length;
    	double[][] C = new double[rowA][colB];
    	double[][] oracle = { {29.0,40.0}, {18.0,32.0}, {19.0,40.0} };
    	
    	assertTrue(Matrix.MatrixMult(rowA,colA,A,rowB,colB,B,C,7));
    	int row = 0;
    	for (double[] arr : C) {
    		assertTrue(Arrays.equals(arr,oracle[row++]));
        }
	}
	
	@Test
	public void testInvalidSize() {
		double[][] A = { {1,6,5}, {2,4,2}, {5,3,2} };
    	double[][] B = { {1,4}, {2,4}, {3,2}, {1,2} };
    	int rowA = A.length, colA = A[0].length, rowB = B.length, colB = B[0].length;
    	double[][] C = new double[rowA][colB];
    	
    	assertFalse(Matrix.MatrixMult(rowA,colA,A,rowB,colB,B,C,4));
	}

}
