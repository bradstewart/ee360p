import java.util.concurrent.*;

/**
 * 
 * @author Brad Stewart (bjs2639), Chris McNair (cm38329)
 * 
 * Parallel/multi-threaded implementation of merge sort. Client code should 
 *   access functionality via the parallelSort static method.
 *
 */
public class PSort implements Runnable {
	public static ExecutorService threadPool = Executors.newCachedThreadPool();
	private static int[] array;
	private int begin, end;
	
	PSort(int[] A, int begin, int end) {
		array = A;
		this.begin = begin;
		this.end = end;	
	}
	
	/**
	 * Sort a specified section of a given array of integers using multiple threads.
	 * 
	 * @param A			integer array, will also hold result
	 * @param begin		beginning index of desired section
	 * @param end		ending index of desired section
	 */
	public static void parallelSort(int[] A, int begin, int end) {
	
		if ((end - begin - 1) < 1) {
			return;
		} else if ((end - begin - 1) == 1) {
			// array of size 2
			if (A[begin] > A[end-1]) {
				swap(A,begin,end-1);
			}
			return;
		} else {
		   int middle = begin + (end-begin)/2;
		   Future<?> f1 = threadPool.submit(new PSort(A, begin, middle+1));
		   Future<?> f2 = threadPool.submit(new PSort(A, middle+1, end));
		   //Thread t1 = new Thread(new PSort(A, begin, middle+1));
		   //Thread t2 = new Thread(new PSort(A, middle+1, end));
		   //t1.start();
		   //t2.start();
		   
		   try {
		   		f1.get();
		   		f2.get();
		   } catch (Exception e) {}
		   
		   merge(A, begin, middle, end);
		}
   	}
	
	private static void merge(int[] A, int begin, int middle, int end) {
		int iLeft = begin, iRight = middle + 1; // check corner cases on this
		int index = 0, length = end-begin;
		int[] copy = new int[length];
		
		while ((iLeft <= middle) && (iRight < end)) {
			copy[index++] = (A[iLeft] < A[iRight]) ? A[iLeft++] : A[iRight++];
		}
		
		while (iLeft <= middle) {			// no more elements in right half
			copy[index++] = A[iLeft++];
		}
		
		while (iRight < end) {				// no more elements in left half
			copy[index++] = A[iRight++];
		}
		System.arraycopy(copy,0,A,begin,length);
	}
	
	private static void swap(int[] A, int left, int right) {
		int tmp = A[left];
		A[left] = A[right];
		A[right] = tmp;
	}
	
	@Override
	public void run() {
		parallelSort(array,begin,end);
	}
	
	/**
	 * Fill the given array randomly with integers between 0 and 9
	 * @param A
	 */
	public static void populateArray(int[] A) {
	    for (int i=0; i<A.length; i++){
	        int n = (int)(Math.random()*9 + 1);
	        A[i] = n;
	    }
	}
	
	public static void main(String[] args) {
		int[] A = new int[10000];
		populateArray(A);
		
		Thread t1 = new Thread(new PSort(A,0,A.length));
		t1.start();
		
		try {
			t1.join();
		} catch (Exception e) {}

	}
}
