import java.net.*;
import java.io.*;
import java.util.*;

public class TCPServerThread extends Thread {
	Library lib;
	Socket theClient;
	String client, book, commandType;
	int sleepTime;
	
	public TCPServerThread(Library lib, Socket s) {
		this.lib = lib;
		this.theClient = s;
	}
	
	private String doCommand() throws InterruptedException {
		boolean returnStatus;
		
		if (commandType.equals("reserve")) {
			returnStatus = lib.reserveBook(client,book);
		} else if (commandType.equals("return")) {
			returnStatus = lib.returnBook(client,book);
		} else {
			Thread.sleep(sleepTime);
			String response = "Slept";
			return response;
		}
		
		if (returnStatus)
			return client + " " + book;
		else
			return "fail " + client + " " + book;
	}
	
	public void run() {
		Scanner sc = null;
		Scanner st = null;
		PrintWriter pout = null;
		
		try {
			sc = new Scanner(theClient.getInputStream());
            pout = new PrintWriter(theClient.getOutputStream());
            String command = sc.nextLine();
            st = new Scanner(command);
            client = st.next();
            book = st.next();
            
            if (book.charAt(0) == 'b') {
            	commandType = st.next();
            	
            } else {
            	commandType = "sleep";
            	sleepTime = Integer.parseInt(book);
            }
            
            String ret = doCommand();
            pout.println(ret);
            pout.flush();
            theClient.close();
            
		} catch (Exception e) {
			
		} finally {
			sc.close();
		}
	}
}
