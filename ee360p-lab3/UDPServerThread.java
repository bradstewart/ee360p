import java.io.ByteArrayInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.StringTokenizer;


public class UDPServerThread extends Thread {
	Library lib;
	DatagramSocket socket;
	DatagramPacket receivepacket, returnpacket;
	byte[] buf = new byte[UDPServer.len];
	int clientNo, bookNo, sleepTime;
	String commandType;
	
	UDPServerThread(Library lib, DatagramPacket pac, DatagramSocket soc) {
		this.lib = lib;
		this.receivepacket = pac;
		this.socket = soc;
	}
	
	@Override
	public void run() {
		try {
			String command = packetToString(receivepacket);
			parseCommand(command);
			
			if (commandType != null){
				String response = doCommand();
				byte[] buffer = new byte[response.length()];
				buffer = response.getBytes();
				
				returnpacket = new DatagramPacket(
						buffer, buffer.length,
						receivepacket.getAddress(),
						receivepacket.getPort());
				socket.send(returnpacket);
			} else {
				Thread.sleep(sleepTime);
				String response = "Slept";
				byte[] buffer = new byte[response.length()];
				buffer = response.getBytes();
				
				returnpacket = new DatagramPacket(
						buffer, buffer.length,
						receivepacket.getAddress(),
						receivepacket.getPort());
				socket.send(returnpacket);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	public static String packetToString(DatagramPacket rpack) {
		ByteArrayInputStream byteIn = new ByteArrayInputStream(rpack.getData(), 0, rpack.getLength());
        byte[] shorter = new byte[byteIn.available()];
        byteIn.read(shorter,0,byteIn.available());
        return new String(shorter);
	}
	
	public String doCommand() {
		boolean returnStatus;
		if (commandType.equals("reserve")) {
			returnStatus = lib.reserveBook(clientNo,bookNo);
		} else {
			returnStatus = lib.returnBook(clientNo,bookNo);
		}
		
		if (returnStatus)
			return "c" + clientNo + " b" + bookNo;
		else
			return "fail c" + clientNo + " b" + bookNo;
	}
	
	void parseCommand(String command) {
		StringTokenizer st = new StringTokenizer(command);
		String ci = st.nextToken();
		clientNo = Character.getNumericValue(ci.charAt(1));
		
		String bi = st.nextToken();
		
		if (bi.charAt(0) == 'b') {								// either a return or reserve
			bookNo = Character.getNumericValue(bi.charAt(1));
			commandType = st.nextToken();
		} else {
			sleepTime = Integer.parseInt(bi);
		}
		
	}
	
}
