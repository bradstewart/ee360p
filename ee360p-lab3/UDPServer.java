import java.net.*;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.io.*;

public class UDPServer {
	static int numberOfServers = 0;
	static int numberOfBooks = 0;
	static String ipa;
	static int port;
	static int len = 1024;
	Library lib;
	
	public UDPServer(int numBooks) {
		lib = new Library(numBooks);
	}
	
	public static void main(String[] args) throws Exception {
		UDPServer server = null;
		DatagramSocket datasocket = null;
		
		try {
			if (args.length != 1) {
				System.out.println("Wrong number of arguments");
				return;
			}
			readInputFile(args[0]);
			
			server = new UDPServer(numberOfBooks);
			datasocket = new DatagramSocket(port);
			
			try {
				while (true) {
				 
					byte[] buf = new byte[len];
					DatagramPacket datapacket = new DatagramPacket(buf, buf.length);
					datasocket.receive(datapacket);
//					System.out.println("Received..");
					Thread t = new UDPServerThread(server.lib,datapacket, datasocket);
					t.start();
				}
		
			} catch (IOException e) {
		        System.err.println(e);
		    } finally {
		    	datasocket.close();
		    }
			
		} catch (BindException e) {
			System.out.println("A server is already running on this port");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		
	}
	
	private static void readInputFile(String file) throws IOException {
		BufferedReader dIn = null;
		try {
            dIn = new BufferedReader(
                                new FileReader(file));
            StringTokenizer st = new StringTokenizer(dIn.readLine());
            numberOfServers = Integer.parseInt(st.nextToken());
            numberOfBooks = Integer.parseInt(st.nextToken());
            String[] locator = dIn.readLine().split(":");
            ipa = locator[0];
            port = Integer.parseInt(locator[1]);
        } catch (FileNotFoundException e) {
            
        } catch (IOException e) {
            System.err.println(e);
        } finally {
        	dIn.close();
        }
	}
}

