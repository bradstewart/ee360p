import java.net.*;
import java.util.StringTokenizer;
import java.io.*;
import java.util.*;

public class UDPClient {
	static String ipa;
	static int port;
	static int len = 1024;
	static DatagramPacket sPacket, rPacket;
	static int numIPA;
	static List<String>  commands = new ArrayList<String>();
	
	public static void main(String args[]) throws Exception {
		if (args.length != 1) {
			System.out.println("Wrong nubmer of arguments");
			return;
		}
		readInputFile(args[0]);
		
		DatagramSocket datasocket = null;
		try {
			InetAddress ipa = InetAddress.getByName("localhost");
			datasocket = new DatagramSocket();
			
			for (String msg : commands) {
				try {
					// send command
					byte[] buffer = new byte[msg.length()];
					buffer = msg.getBytes();
					sPacket = new DatagramPacket(buffer, buffer.length, ipa, port);
			        datasocket.send(sPacket);
			        
			        // receive command
			        byte[] rbuffer = new byte[len];
                    rPacket = new DatagramPacket(rbuffer, rbuffer.length);
                    datasocket.receive(rPacket);
                    
                    String retstring = packetToString(rPacket);
                    if (!retstring.equals("Slept")) {				// print if not a sleep command
                    	System.out.println(retstring);
                    }
                    
				}  catch (IOException e) {
			        System.err.println(e);
			    }
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			datasocket.close();
		}
	}
	
	public static String packetToString(DatagramPacket rpack) {
		ByteArrayInputStream byteIn = new ByteArrayInputStream(rpack.getData(), 0, rpack.getLength());
        byte[] shorter = new byte[byteIn.available()];
        byteIn.read(shorter,0,byteIn.available());
        return new String(shorter);
	}
	
	private static void readInputFile(String file) throws IOException {
		BufferedReader dIn = null;
		try {
            dIn = new BufferedReader(
                                new FileReader(file));
            numIPA = Integer.parseInt(dIn.readLine());
            String[] locator = dIn.readLine().split(":");
            ipa = locator[0];
            port = Integer.parseInt(locator[1]);
            String line = dIn.readLine();
            while (line != null) {
            	commands.add(line);
            	line = dIn.readLine();
            }
            
        } catch (FileNotFoundException e) {
            
        } catch (IOException e) {
            System.err.println(e);
        } finally {
        	dIn.close();
        }
	}
}
