import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.io.*;

public class TCPClientMulti {
	static final int TIMEOUT = 900;
	Scanner din;
    PrintStream pout;
    Socket server;
//    String ipa;
//	int port;
	int numOfServers;
	List<Host> servers = new ArrayList<Host>();
	List<String> commands = new ArrayList<String>();
	
	TCPClientMulti(String file) {
		try {
			this.readInputFile(file);
		} catch (IOException e) {
			System.out.println("Could not read input file");
		}
	}
    
    public static void main(String[] args) throws IOException {
    	if (args.length != 1) {
			System.out.println("Wrong nubmer of arguments");
			return;
		}
    	TCPClientMulti myClient = new TCPClientMulti(args[0]);
        
        try {
        	for (String msg : myClient.commands) {
				try {
					// send command
					myClient.sendCommand(msg);
				}  catch (IOException e) {
			        System.err.println(e);
			    }
			}
        } catch (Exception e) {
            System.err.println("Client aborted: " + e);
        }
    }
    
    public void getSocket(String ipa, int port) throws IOException {
        server = new Socket(ipa, port);
        din = new Scanner(server.getInputStream());
        pout = new PrintStream(server.getOutputStream());
    }
    
    public void sendCommand(String command) throws IOException {
    	Socket serversock = null;
    	
    	// Try each host in a round-robin fashion. Go to next after timeout
    	int i = 0;
    	Host h = servers.get(i);
    	while (true) {
    		try {
				serversock = new Socket();   
				serversock.connect(new InetSocketAddress(h.name,h.port),TIMEOUT);
				din = new Scanner(serversock.getInputStream());
				pout = new PrintStream(serversock.getOutputStream());
				// send command to server
		    	pout.println(command);
		    	pout.flush();
		    	
		    	// get and print the return value
		    	String retValue = din.nextLine();
		    	if (!retValue.equals("Slept")) {
		    		System.out.println(retValue);
		    	}
		    	serversock.close();
				break;
			} catch(NoSuchElementException e) {		// Connected to server but then the server closed the connection
				System.out.println("Connected to server but then the server closed the connection.");
				i = (i+1) % numOfServers;
				h = servers.get(i);
				continue;
			} catch (Exception e) {					// The server is down
//				System.out.println("Could not connect to " + h.port);
				i = (i+1) % numOfServers;
				h = servers.get(i);
				continue;
			}
    	}
    }
    
    private void readInputFile(String file) throws IOException {
		BufferedReader dIn = null;
		try {
            dIn = new BufferedReader(new FileReader(file));
            numOfServers = Integer.parseInt(dIn.readLine());
            
            // populate list of servers
            String line = dIn.readLine();
            while(line != null && line.charAt(0) != 'c') {
            	String[] s = line.split(":");
            	servers.add( new Host(s[0], Integer.parseInt(s[1])));
            	line = dIn.readLine();
            } 
            
            // populate list of commands
            while (line != null) {
            	commands.add(line);
            	line = dIn.readLine();
            }
            
        } catch (FileNotFoundException e) {
            
        } catch (IOException e) {
            System.err.println(e);
        } finally {
        	dIn.close();
        }
	}
}
