import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.io.*;

public class TCPClient {
	Scanner din;
    PrintStream pout;
    Socket server;
    String ipa;
	int port;
	int len = 1024;
	int numIPA;
	List<String> commands = new ArrayList<String>();
    
    public void getSocket() throws IOException {
        server = new Socket(ipa, port);
        din = new Scanner(server.getInputStream());
        pout = new PrintStream(server.getOutputStream());
    }
    
    public String reserve(String ci, String bi) throws IOException {
    	getSocket();
    	pout.println(ci + " " + bi + " reserve");
    	pout.flush();
    	String retValue = din.nextLine();
    	System.out.println(retValue);
    	server.close();
    	return retValue;
    }
    
    public void sendCommand(String command) throws IOException {
    	getSocket();
    	pout.println(command);
    	pout.flush();
    	String retValue = din.nextLine();
    	if (!retValue.equals("Slept")) {
    		System.out.println(retValue);
    	}
    	server.close();
    }
    
    public static void main(String[] args) throws IOException {
    	if (args.length != 1) {
			System.out.println("Wrong nubmer of arguments");
			return;
		}
    	TCPClient myClient = new TCPClient();
		myClient.readInputFile(args[0]);
		
        
        try {
        	for (String msg : myClient.commands) {
				try {
					// send command
					myClient.sendCommand(msg);
				}  catch (IOException e) {
			        System.err.println(e);
			    }
			}
        } catch (Exception e) {
            System.err.println("Client aborted: " + e);
        }
    }
    
    private void readInputFile(String file) throws IOException {
		BufferedReader dIn = null;
		try {
            dIn = new BufferedReader(
                                new FileReader(file));
            numIPA = Integer.parseInt(dIn.readLine());
            String[] locator = dIn.readLine().split(":");
            ipa = locator[0];
            port = Integer.parseInt(locator[1]);
            String line = dIn.readLine();
            while (line != null) {
            	commands.add(line);
            	line = dIn.readLine();
            }
            
        } catch (FileNotFoundException e) {
            
        } catch (IOException e) {
            System.err.println(e);
        } finally {
        	dIn.close();
        }
	}
}
