import java.net.*;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.io.*;

public class TCPServer {
	static int numberOfServers = 0;
	static int numberOfBooks = 0;
	static String ipa;
	static int port;
	static int len = 1024;
	Library lib;
	
	TCPServer(int numBooks) {
		lib = new Library(numBooks);
	}
	
	public static void main(String[] args) throws Exception {
		ServerSocket listener = null;
		Socket s;
		try {
			if (args.length != 1) {
				System.out.println("Wrong number of arguments");
				return;
			}
			readInputFile(args[0]);
			
			TCPServer server = new TCPServer(numberOfBooks);
			listener = new ServerSocket(port);
			
			try {
				while ( (s = listener.accept()) != null) {
					Thread t = new TCPServerThread(server.lib,s);
					t.start();
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				listener.close();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		
	}
	
	private static void readInputFile(String file) throws IOException {
		BufferedReader dIn = null;
		try {
            dIn = new BufferedReader(
                                new FileReader(file));
            StringTokenizer st = new StringTokenizer(dIn.readLine());
            numberOfServers = Integer.parseInt(st.nextToken());
            numberOfBooks = Integer.parseInt(st.nextToken());
            String[] locator = dIn.readLine().split(":");
            ipa = locator[0];
            port = Integer.parseInt(locator[1]);
        } catch (FileNotFoundException e) {
            
        } catch (IOException e) {
            System.err.println(e);
        } finally {
        	dIn.close();
        }
	}
}
