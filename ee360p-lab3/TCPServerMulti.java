import java.net.*;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.io.*;

public class TCPServerMulti {
	static final int TIMEOUT = 900;
	static int numberOfServers = 0;
	static int numberOfBooks = 0;
	static List<Host> servers = new ArrayList<Host>();
	static List<String> commands = new ArrayList<String>();
	String ipa;
	int port;
	Library lib;
	final int ID;
	int K_MESSAGES;
	int SLEEP_TIME;
	AtomicInteger clientMessagesReceived = new AtomicInteger(0);
	LamportMutex lm;
	boolean timeToSleep = false;
	ServerSocket listener = null;

	TCPServerMulti(int numBooks, int id) {
		this.lib = new Library(numberOfBooks);
		this.ID = id;
		
		Host host = servers.get(id-1);
        ipa = host.name;
        port = host.port;
        lm = new LamportMutex(servers,ID);
        
        for (String c : commands) {
        	int serverId = Character.getNumericValue(c.charAt(1));
        	if ( serverId == this.ID) {
        		StringTokenizer st = new StringTokenizer(c);
        		st.nextToken();		// discard si
        		K_MESSAGES = Integer.parseInt(st.nextToken());
        		SLEEP_TIME = Integer.parseInt(st.nextToken());
        	}
        }
        
        // Set to max value if there is no command for this server
        if (K_MESSAGES == 0) K_MESSAGES = Integer.MAX_VALUE;
        
        // start the listener
        try {
			listener = new ServerSocket(port);
		} catch (IOException e1) {
			System.err.println("Could not listen on port." + port);
			e1.printStackTrace();
			System.exit(1);
		}
        
        // Get Library from any server that is up
        Thread t = new RebootWorker(this);
		t.start();
	}
	
	public static void main(String[] args) throws Exception {
		
		Socket s;
		try {
			if (args.length != 2) {
				System.out.println("Wrong number of arguments");
				return;
			}
			
			// parse arguments and read input file
			int serverId = Integer.parseInt(args[1]);
			readInputFile(args[0], serverId);
			
			TCPServerMulti myServer = new TCPServerMulti(numberOfBooks, serverId);
			
			try {
				// wait for requests and start a worker thread when accepted
				while (true) {
					if (myServer.timeToSleep) {		// sleep then reboot
						System.out.println("Sleeping...");
						myServer.listener.close();
						Thread.sleep(myServer.SLEEP_TIME);
						myServer.listener = new ServerSocket(myServer.port);
						Thread t = new RebootWorker(myServer);
						t.start();
					}
					
					System.out.println(myServer.lib.printLibrary());
					s = myServer.listener.accept();
					
					// Start worker to handle the request
					Thread t = new TCPServerMultiThread(s,myServer);
					t.start();
					System.out.println("Starting worker on server " + myServer.ID);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				myServer.listener.close();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
	}
	
	public void registerClientMessage() {
//		System.out.println("Register client request");
//		clientMessagesReceived++;
	}

	private static void readInputFile(String file, int serverId) throws IOException {
		BufferedReader dIn = null;
		try {
            dIn = new BufferedReader(new FileReader(file));
            
            // parse the first line
            StringTokenizer st = new StringTokenizer(dIn.readLine());
            numberOfServers = Integer.parseInt(st.nextToken());
            numberOfBooks = Integer.parseInt(st.nextToken());
            
            // populate list of servers_ipa_port
            int i = 0;
            String line = dIn.readLine();
            while(line != null && line.charAt(0) != 's') {
            	String[] s = line.split(":");
            	servers.add( new Host(s[0], Integer.parseInt(s[1])));
            	line = dIn.readLine();
            }  
            
            // populate the list of server commands
            while(line != null) {
            	commands.add(line);
            	line = dIn.readLine();
            }
            
        } catch (FileNotFoundException e) {
            
        } catch (IOException e) {
            System.err.println(e);
        } finally {
        	dIn.close();
        }
	}

}


