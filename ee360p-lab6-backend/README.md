ee360p-lab6-backend
======

## API Explorer
Available at /_ah/api/explorer on both localhost and http://brad-chris-ee360p.appspot.com.

## Running Locally
You need Eclipse.
Install the Google plugin for eclipse.
Import the project into your Eclipse workspace.
Debug it as a google web application.
