package com.ee360p.lab6;

import java.io.IOException;
import javax.servlet.http.*;

/**
 * 
 * @author Brad Stewart
 * Auto generated servlet, exists solely for testing purposes.
 *
 */
@SuppressWarnings("serial")
public class Ee360p_lab6Servlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/plain");
		resp.getWriter().println("Hello, world");
	}
}
