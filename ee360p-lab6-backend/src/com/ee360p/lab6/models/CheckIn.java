/**
 * 
 */
package com.ee360p.lab6.models;

import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import static com.ee360p.lab6.OfyService.ofy;

/**
 * Objectify entity class to be serialized to the Google Datastore.
 * @author Brad Stewart
 *
 */
@Entity
public class CheckIn {
	
	/* Class attributes */
	
	@Id private Long id;
	@Index private Double lon;
	@Index private Double lat;
	private String user;
	
	/* Constructors and getters */
	
	public CheckIn(String username, Double latitude, Double longitude) {
		this.user = username;
		this.lat = latitude;
		this.lon = longitude;
	}
	
	public CheckIn() {	}

	public Double getLatitude() { return this.lat; }
	public Double getLongitude() { return this.lon; }
	public Long getId() { return this.id; }
	public String getusername() { return this.user; }

	/* Static class-level methods for retreiving instances from the Datastore */
	
	public static List<CheckIn> all() {
		return ofy().load().type( CheckIn.class ).list();
	}
	
	public static CheckIn get( Integer id ) {
		Long longId = new Long( id );
		return get( longId );
	}
	
	public static CheckIn get( Long id ) {
		CheckIn c = ofy().load().type( CheckIn.class ).id( id ).now();
		System.out.println( c.toString() );
		return c;
	}
	
	/* Instance methods for interacting with the model object */
	
	public CheckIn save() {
		ofy().save().entity( this ).now();
		return this;
	}
	
	/* Overriden methods */
	
	@Override
	public String toString() {
		return "Id: "+ this.id.toString() + "Latitude: " + this.lat.toString() + "Longitude: " + this.lon.toString();
	}

}
