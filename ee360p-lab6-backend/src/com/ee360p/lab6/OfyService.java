package com.ee360p.lab6;

import com.ee360p.lab6.models.*;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;

/**
 * Objectify service for registering all Entity classes.
 * @author Brad Stewart
 *
 */
public class OfyService {
	
	static {
		factory().register( CheckIn.class );
	}

	public static Objectify ofy() {
	    return ObjectifyService.ofy();
	}

	public static ObjectifyFactory factory() {
	    return ObjectifyService.factory();
	}
}