package com.ee360p.lab6;

import com.ee360p.lab6.models.CheckIn;

/**
 * Haversine formula for calculating distance between two lat/lon coordinates.
 *
 */
public class Distance {

	/**
	 * Public method for calculating the distance.
	 * @param c1
	 * @param c2
	 * @param unit
	 * @return
	 */
	public static final double calculate(CheckIn c1, CheckIn c2, char unit) {
	    double theta = c1.getLongitude() - c2.getLongitude();
	    double dist = Math.sin(deg2rad(c1.getLatitude())) * Math.sin(deg2rad(c2.getLatitude())) + Math.cos(deg2rad(c1.getLatitude())) * Math.cos(deg2rad(c2.getLatitude())) * Math.cos(deg2rad(theta));
	    dist = Math.acos(dist);
	    dist = rad2deg(dist);
	    dist = dist * 60 * 1.1515;

	    if (unit == 'K') {
	        dist = dist * 1.609344;
	    }
	    else if (unit == 'N') {
	        dist = dist * 0.8684;
	    }

	    return (dist);
	}

	/**
	 * <p>This function converts decimal degrees to radians.</p>
	 * 
	 * @param deg - the decimal to convert to radians
	 * @return the decimal converted to radians
	 */
	private static final double deg2rad(double deg) {
	    return (deg * Math.PI / 180.0);
	}

	/**
	 * <p>This function converts radians to decimal degrees.</p>
	 * 
	 * @param rad - the radian to convert
	 * @return the radian converted to decimal degrees
	 */
	private static final double rad2deg(double rad) {
	    return (rad * 180 / Math.PI);
	}

}