package com.ee360p.lab6;

import java.util.Comparator;

import com.ee360p.lab6.models.CheckIn;

public class CheckInDistanceComparator implements Comparator<CheckIn> {
	
	private CheckIn base; 

	public CheckInDistanceComparator( Double latitude, Double longitude ) {
		base = new CheckIn( "base", latitude, longitude );
	}
	
	@Override
	public int compare(CheckIn arg0, CheckIn arg1) {
		return Double.compare(Distance.calculate(base, arg0, 'K'), 
									Distance.calculate(base, arg1, 'K'));
	}


}
