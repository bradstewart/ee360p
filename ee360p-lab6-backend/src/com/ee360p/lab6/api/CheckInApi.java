/**
 * checkinapi.java
 */
package com.ee360p.lab6.api;

import java.util.Collections;
import java.util.List;

import javax.inject.Named;

import com.ee360p.lab6.CheckInDistanceComparator;
import com.ee360p.lab6.models.CheckIn;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiMethod.HttpMethod;

/**
 * Core API methods accessible at http://brad-chris-ee360p.appspot.com/api.
 * @author Brad Stewart
 *
 */
@Api( name = "api", version = "v1")
public class CheckInApi {
	
	/**
	 * GET root/checkins for a listing of all checkins. Returned as JSON under the key "items".
	 * @return
	 */
	@ApiMethod( name = "checkins.index", path = "checkins", httpMethod = HttpMethod.GET )
	public List<CheckIn> index( ) {
		return CheckIn.all();
	}
	
	/**
	 * GET root/checkin will return a JSON representation of the checkin with the supplied Id.
	 * @param Id
	 * @return
	 */
	@ApiMethod( name = "checkins.show", path = "checkin", httpMethod = HttpMethod.GET )
	public CheckIn show( @Named("id") Integer Id ) {
		return CheckIn.get( Id );
	}
	
	/**
	 * POST root/checkins with the required parameters to create a new checkin.
	 * @param username
	 * @param latitude
	 * @param longitude
	 * @return
	 */
	@ApiMethod( name = "checkins.create", path = "checkins", httpMethod = HttpMethod.POST )
	public CheckIn create( @Named("username") String username,
						   @Named("latitude") Double latitude,
						   @Named("longitude") Double longitude) {	
		
		CheckIn created = new CheckIn( username, latitude, longitude ).save();
		return created;
	}
	
	/**
	 * GET root/nearby returns at most 5 of the nearest checkins under the JSON key "items".
	 * @param latitude
	 * @param longitude
	 * @return
	 */
	@ApiMethod( name = "checkins.nearby", path = "nearby", httpMethod = HttpMethod.GET )
	public List<CheckIn> nearby( @Named("latitude") Double latitude,
			   					 @Named("longitude") Double longitude) {
		
		List<CheckIn> all = CheckIn.all();
		Collections.sort( all, new CheckInDistanceComparator( latitude, longitude ) );
		
		if ( all.size() >= 5 ) {
			return all.subList(0, 5);
		} else {
			return all;
		}
		
	}
	
}
